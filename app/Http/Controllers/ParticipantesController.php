<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Participante;


class ParticipantesController extends Controller
{
    //AJAX
    public function buscar(Request $request)
    {              
        return json_encode
        (
            Participante::select('tutor')
            ->where('tutor','like','%'.$request->busqueda.'%')
            ->distinct()
            ->get()
        );
    }

    public function getInscribir()
    {
    	return view('participantes.crear');
    }

    public function postInscribir(Request $request)
    {
    	$nombre=$request->nombre;
    	$ape=$request->apellidos;
    	$centro=$request->centro;
    	$tutor=$request->tutor;
    	$fechaNac=$request->fechaNac;
    	$mod=$request->modalidad;
        define('PARAMETROS',isset($nombre)&&isset($ape)&&isset($centro)&&isset($tutor) &&isset($fechaNac)&&isset($mod));
    	if(PARAMETROS)
        {
            //PARTICIP
            $p=new Participante();
            $p->nombre=$nombre;
            $p->apellidos=$ape;
            $p->centro=$centro;
            $p->tutor=$tutor;
            $p->fechaNacimiento=$fechaNac;
            $p->modalidad_id=$mod;
            if(!empty($request->imagen && $request->imagen->isValid())) 
            {           
                $p->imagen = $request->imagen->store('', 'participantes');      
            }
            try
            {
                if($p->save())  
                    return redirect('/');
            }
            catch(\Illuminate\Database\QueryException $ex)
            {
                return redirect('/');
            }
        }        
    }
}
