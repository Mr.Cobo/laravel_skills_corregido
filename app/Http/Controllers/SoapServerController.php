<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\lib\WSDLDocument;
use App\Participante;
use App\Modalidad;
use SoapServer;

class SoapServerController extends Controller
{
	private $clase="\\App\\Http\\Controllers\\SkillsWebService";
	private $uri="http://127.0.0.1/laravel_skills_Candido/public/api";
	private $urlWSDL="http://127.0.0.1/laravel_skills_Candido/public/api/wsdl";

    public function getServer()
    {
    	$server = new SoapServer($this->urlWSDL);
		$server -> setClass($this->clase);
		$server -> handle();
		exit();
    }

    public function getWSDL()
    {
    	$wsdl = new WSDLDocument($this->clase,$this->uri,$this->uri);
		$wsdl->formatOutput = true;
		header("Content-type: text/xml");
		echo $wsdl->saveXML();
    }
}

class SkillsWebService
{
	/**
	 * 
	 * @param string $centro 
	 * @return int
	 */
	public function getNumeroParticipantesCentro($centro)
	{
		return Participante::where('centro',$centro)				
				->count();
	}

	/**
	 * 
	 * @param string $tutor 
	 * @return App\Participante[]
	 */
	public function getParticipantesTutor($tutor)
	{
		return Participante::where('tutor',$tutor)
				->orderBy('puntos','desc')
				->get();
	}
}