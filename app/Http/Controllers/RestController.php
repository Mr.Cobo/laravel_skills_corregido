<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modalidad;
use App\Participante;

class RestController extends Controller
{
    public function getGanador($slug)
    {
    	$m = Modalidad::select('id')
	    	->where('slug',$slug)
	    	->firstOrFail();
	    	    
    	return response()->json(
    		Participante::where('modalidad_id',$m->id)
            ->orderBy('puntos','desc')
	    	->firstOrFail());
    }
}
