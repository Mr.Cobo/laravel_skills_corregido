<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modalidad;
use App\Participante;

class ModalidadesController extends Controller
{
    public function getModalidades()
    {
    	return view('modalidades.index');
    }

    public function getMostrar($slug)
    {
    	$m = Modalidad::where('slug',$slug)->firstOrFail();    	
    	return view('modalidades.mostrar',array('m'=>$m));
    }

    public function getPuntuar($slug)
    {
    	$m = Modalidad::where('slug',$slug)->firstOrFail();
    	foreach ($m->participantes as $p) 
    	{
    		$p->puntos = rand(0,100);
    		$p->save();
    	}    	
    	return redirect('modalidades/mostrar/'.$slug);
    }

    public function getReset($slug)
    {
    	$m = Modalidad::where('slug',$slug)->firstOrFail();
    	foreach ($m->participantes as $p) 
    	{
    		$p->puntos = -1;
    		$p->save();
    	}    	
    	return redirect('modalidades/mostrar/'.$slug);
    }
}
