@extends('layouts.master')
@section('titulo')
MODALIDADES
@endsection
@section('contenido')
	@inject('m','App\Modalidad')
	<div class="row">
		@foreach($m::all() as $modalidad)
			<div class="col-md-4" style="border:2px solid black">
				<img src="{{url('assets/imagenes/modalidades')}}
					/{{$modalidad->imagen}}" width="40" height="40">
				<h1>
					<a href="{{url('/modalidades/mostrar')}}/{{$modalidad->slug }}">
						{{ $modalidad->nombre }}
					</a>
				</h1>
				<strong>
					{{ $modalidad->participantes->count() }}
				</strong>
			</div>
		@endforeach
	</div>
@endsection