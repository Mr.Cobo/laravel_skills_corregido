@extends('layouts.master')
@section('titulo')
MODALIDADES
@endsection
@section('contenido')
	<h1>{{$m->nombre}}</h1>
	<h2>Familia profesional: {{$m->familiaProfesional}}</h2>
	<h2>Participantes:</h2>
	<div class="row" >
		@foreach($m->participantes as $participante)
			<div class="col-md-4">
				{{$participante->nombre}}<br>
				<img src="{{asset('assets/imagenes/participantes')}}/
				{{$participante->imagen}}" width="100" height="100">
			</div>
		@endforeach
	</div><br>
	<a href="{{url('modalidades/puntuar')}}/{{$m->slug}}">
		Puntuar
	</a>	
	<a href="{{url('modalidades/resetear')}}/{{$m->slug}}">
		Resetear
	</a>
	<br>
	<h2>Resultados:</h2>
	<div class="table">
		@php
			$todoOk = false;
		@endphp
		@foreach($m->participantes as $p)
			@if($p->puntos != -1)
				@php
					$todoOk = true;
				@endphp
			@endif
		@endforeach
		@if($todoOk)
		<table>
			<thead>
				<td>Nombre</td>
				<td>Puntos</td>
			</thead>
			@foreach($m->participantesOrderByPuntos as $participante)
					<tr>
						<td>{{ $participante->nombre }}</td>
						<td>{{ $participante->puntos }}</td>
					</tr>								
			@endforeach
		</table>
		@else
			<h1>Todos los usuarios tienen puntos -1 !!</h1>
		@endif
	</div>
@endsection