@extends('layouts.master')
@section('titulo')
INSCRIBIR
@endsection
@section('contenido')
	<div class="row">
  	<div class="offset-md-3 col-md-6">
  		<div class="card">
  			<div class="card-header text-center">
  				Añadir nuevo participante
  			</div>
  			<div class="card-body" style="padding:30px">
          <!--FORMULARIO-->
  			<form method="POST" action="{{ url('participantes/inscribir') }}" enctype="multipart/form-data">
  					{{ csrf_field() }}
            <!--nombre-->
  				<div class="form-group">
  						<label>Nombre</label>
  						<input type="text" name="nombre" class="form-control" required>
  					</div>
            <!---->
  			<div class="form-group">
              <label>Apellidos</label>
              <input type="text" name="apellidos" class="form-control" required>
  			</div>
            <!---->
  			<div class="form-group">
              <label>Centro</label>
  			     	<input type="text" name="centro" class="form-control" required>
  			</div>
        <!--TUTOR-->
  			<div class="form-group">
  				<label>Tutor</label>
  				<input id="busqueda" type="text" name="tutor" class="form-control" required>
          <div id="log" style="height: 100px; width: 300px;" class="ui-widget-content"></div>
        </div>
  			</div>
        <!---->
  			<div class="form-group">
  				<label>Fecha nacimiento</label>
  				<input type="date" name="fechaNac" required>
  			</div>
  			<div class="form-group">
  				<label>Modalidad</label>
  				@inject('m', 'App\Modalidad')
  				  <select name="modalidad" class="form-control" required>
	                @foreach ($m::all() as $modalidad)
	                {
	                    <option value="{{ $modalidad->id }}">
	                    	{{ $modalidad->nombre }}
	                    </option>
	                }
	                @endforeach
            </select>
  			</div>
  			<div class="form-group">
  				<label>Imagen</label>
  				<input type="file" name="imagen" required>
  			</div>
  			<div class="form-group text-center">
  				<button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">
  					Añadir Participante
  					</button>
  			</div>
  			</form>
  			</div>
  		</div>
  	</div>
  </div>
  <!--CODIGO PARA PETICIONES AJAX-->
<script type="text/javascript">
  $(document).ready(function()
  {
    function log( message ) 
    {
       $( "#log" ).scrollTop( 0 );
    }

    $("#busqueda").autocomplete(
     {
        source: function( request, response ) 
        {
            var formulario = $( "#busqueda" ).val();
            $.ajax
            ({
                url : 'http://127.0.0.1/laravel_skills_Candido/public/busquedaAjax',
                type : 'POST',
                data : 
                {
                  "_token":"{{ csrf_token() }}",
                  "busqueda":formulario
                },
                dataType : 'json',
                success : function( json )
                {            
                    $( "#log" ).empty();
                    for(let i in json)
                    {
                        console.log(json[i].tutor);
                        $( "#log" ).append(json[i].tutor + "<br>");
                    }
                }
            });
        }
    });
  });
 </script>
@endsection