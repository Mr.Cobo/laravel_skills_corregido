<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',function(){return redirect('/modalidades');});

//MODS
Route::get('modalidades','ModalidadesController@getModalidades');

Route::get('modalidades/mostrar/{slug}','ModalidadesController@getMostrar');

Route::get('modalidades/puntuar/{slug}','ModalidadesController@getPuntuar');

Route::get('modalidades/resetear/{slug}','ModalidadesController@getReset');

//PART
Route::get('participantes/inscribir','ParticipantesController@getInscribir');
Route::post('participantes/inscribir','ParticipantesController@postInscribir');

//SOAP
Route::any('api','SoapServerController@getServer');
Route::any('api/wsdl','SoapServerController@getWSDL');

//REST
Route::get('rest/ganador/{slug}','RestController@getGanador');

//AJAX
Route::post('busquedaAjax','ParticipantesController@buscar');
